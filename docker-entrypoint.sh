#!/bin/bash

export LANG="fr_FR.UTF-8"
export PYTHONIOENCODING=UTF-8
if [ "$1" != "/docker-entrypoint.sh" ]; then
	ERA_OPTIONS="$@"
fi
/usr/share/era/era.sh $ERA_OPTIONS
