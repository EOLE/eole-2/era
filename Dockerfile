FROM ubuntu:focal-20241011
COPY repos/eole.list /etc/apt/sources.list.d/
COPY repos/eole-archive-eole-2.8-focal-fossa.gpg /etc/apt/trusted.gpg.d/
COPY repos/eole-archive-eole-2.9-jammy-jellyfish.gpg /etc/apt/trusted.gpg.d/
RUN export DEBIAN_FRONTEND=noninteractive \
  && apt-get update -y \
  && apt-get install -y era=2.9.0-24 language-pack-fr \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
COPY docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

VOLUME /usr/share/era/modeles/

CMD ["/docker-entrypoint.sh"]
